import { Injectable, Logger } from '@nestjs/common';
import { Mail } from './dtos';
import { MailerService } from '@nestjs-modules/mailer';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@Injectable()
export class MailsenderService {

  logger = new Logger("RedisQueue");
  constructor(
    @InjectQueue('email') private emailQueue: Queue,
  ) {
    this.init();
  }

  async init() {
    try {
      await this.delay(1000, 1);
      this.checkQueueAvailability();
    } catch (e) {
      this.logger.error(e);
    }
  }

  private checkQueueAvailability(): void {
    if (this.emailQueue.client.status === "ready") {
      this.logger.log("Redis is ready");
    } else {
      throw new Error("Redis not available");
    }
  }

  delay(t: number, val: any) {
    return new Promise(function (resolve) {
      setTimeout(function () {
        resolve(val);
      }, t);
    });
  }


  async sendWelcomeEmail(data: Mail) {
    const job = await this.emailQueue.add('welcome', { data });
    return { jobId: job.id };
  }

  async sendResetPasswordEmail(data: Mail) {
    console.log(data);
    // const job = await this.emailQueue.add('reset-password', { data });
    // return { jobId: job.id };
  }
}
