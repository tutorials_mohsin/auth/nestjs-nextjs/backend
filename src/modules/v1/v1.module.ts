import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { MailsenderModule } from './mailsender/mailsender.module';

@Module({
  imports: [AuthModule, MailsenderModule],
})
export class V1Module {}
