import { Strategy, Profile } from 'passport-facebook';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AuthService } from '../auth.service';
import { Auth } from '../entity';

@Injectable()
export class FacebookAuthStrategy extends PassportStrategy(
  Strategy,
  'facebook',
) {
  constructor(
    configService: ConfigService,
    private authService: AuthService,
  ) {
    super({
      clientID: configService.get<string>('FACEBOOK_CLIENT_ID'),
      clientSecret: configService.get<string>('FACEBOOK_CLIENT_SECRET'),
      callbackURL: configService.get<string>('FACEBOOK_CALLBACK_URL'),
      scope: ['email', 'profile'],
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: Profile,
  ): Promise<Auth> {
    try {
      const user = await this.authService.getUserByProviderId(profile.id);
      if (!user) {
        const newUser = await this.authService.registerFacebook(profile);
        if (!newUser) {
          throw new InternalServerErrorException('Failed to create a new user');
        }
        return newUser;
      }
      return user;
    } catch (error) {
      throw new InternalServerErrorException('Failed to validate access token');
    }
  }
}
