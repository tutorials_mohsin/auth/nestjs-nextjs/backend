import {
  HttpException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { CreateAuthDto, LoginAuthDto } from './dtos';
import { Response, Request } from 'express';
import { Auth } from './entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { InvalidCredentials, UniqueViolation } from '../../../common/exceptions';
import { PostgresErrorCode } from '../../../common/enums';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { v4 as uuidv4 } from 'uuid';
import * as bcrypt from 'bcrypt';
import { Profile as GithubProfile } from 'passport-github';
import { Profile as GoogleProfile } from 'passport-google-oauth20';
import { Profile as FacebookProfile } from 'passport-facebook';

import { OAuth2Client } from 'google-auth-library';
import { MailsenderService } from '../mailsender/mailsender.service';

@Injectable()
export class AuthService {
  private oAuth2Client: OAuth2Client;
  constructor(
    @InjectRepository(Auth)
    private readonly authRepository: Repository<Auth>,
    private readonly jwtService: JwtService,
    private readonly mailSenderService: MailsenderService,
    private readonly configService: ConfigService,
  ) {
    this.oAuth2Client = new OAuth2Client(
      this.configService.get('GOOGLE_CLIENT_ID'),
      this.configService.get('GOOGLE_CLIENT_SECRET'),
    );
  }

  public async register(createAuthDto: CreateAuthDto, res: Response) {
    try {
      const auth = await this.authRepository.findOneBy({
        email: createAuthDto.email,
      });
      if (auth) {
        throw new HttpException('User already registered', HttpStatus.CONFLICT);
      }

      const user = this.authRepository.create({
        ...createAuthDto,
        provider: 'local',
      });
      await this.authRepository.save(user);

      const [accessToken, refreshToken] = await this.generateTokens(user);

      await this.setTokens(res, { accessToken, refreshToken });

      this.mailSenderService.sendWelcomeEmail({
        to: user.email,
        subject: 'Welcome',
        text: 'Testing Email',
      });
      return { user, status: 'success' };
    } catch (error) {
      if (error.code == PostgresErrorCode.UniqueViolation) {
        if (error.detail.includes('email')) {
          throw new UniqueViolation('email');
        }
      }
      if (error.status == HttpStatus.CONFLICT) {
        throw new HttpException('User already registered', HttpStatus.CONFLICT);
      }
      throw new InternalServerErrorException();
    }
  }

  public async sendMail() {
    return await this.mailSenderService.sendWelcomeEmail({
      to: 'mohsinamin953@gmail.com',
      subject: 'Welcome to',
      text: 'Welcome'
    });
  }

  public async login(loginAuthDto: LoginAuthDto, response: Response) {
    try {
      const user = await this.getAuthenticatedUser(
        loginAuthDto.email,
        loginAuthDto.password,
      );
      const [accessToken, refreshToken] = await this.generateTokens(user);
      await this.setTokens(response, { accessToken, refreshToken });
      return { user, status: 'success', accessToken, refreshToken };
    } catch (err) {
      console.log(err);
      throw new HttpException(err.response, err.status);
    }
  }

  public async registerGithub(profile: GithubProfile): Promise<Auth> {
    const user = await this.authRepository.save({
      name: profile.displayName,
      email: profile.emails[0]?.value ?? `${profile.id}@github.com`,
      password: `github`,
      provider: profile.provider,
      providerId: profile.id,
    });
    await this.authRepository.save(user);
    return user;
  }

  public async registerGoogle(profile: GoogleProfile): Promise<Auth> {
    const user = await this.authRepository.save({
      name: profile.displayName,
      email: profile.emails[0]?.value ?? `${profile.id}@google.com`,
      password: `google`,
      provider: profile.provider,
      providerId: profile.id,
    });
    await this.authRepository.save(user);
    return user;
  }

  public async registerFacebook(profile: FacebookProfile): Promise<Auth> {
    const user = await this.authRepository.save({
      name: profile.displayName,
      email: profile.emails[0]?.value ?? `${profile.id}@facebook.com`,
      password: `facebook`,
      provider: profile.provider,
      providerId: profile.id,
    });
    await this.authRepository.save(user);
    return user;
  }

  public async googleOneTap(token: string, response: Response) {
    try {
      const ticket = await this.oAuth2Client.verifyIdToken({
        idToken: token,
        audience: this.configService.get('GOOGLE_CLIENT_ID'),
      });
      const payload = ticket.getPayload();
      const email = payload.email;
      const providerId = payload.sub;
      const name = payload.name;

      let user = await this.getUserByProviderId(providerId);
      if (!user) {
        user = await this.getUserByEmail(email);
      }
      if (!user) {
        user = await this.authRepository.save({
          name: name ?? email,
          email: email,
          password: `google`,
          provider: 'google',
          providerId: providerId,
        });
      }
      const [accessToken, refreshToken] = await this.generateTokens(user);

      await this.setTokens(response, { accessToken, refreshToken });

      return { user, status: 'success' };
    } catch (error: any) {
      console.log(error);
      throw new HttpException(error.response, error.status);
    }
  }

  public async forgetPassword(email: string) {
    let user = await this.getUserByField('email', email);
    if (!user) {
      user = await this.getUserByField('phone', email);
    }
    if (!user) {
      throw new NotFoundException('User not found');
    }

    return user.email;
  }

  public async setLoginCookie(request: Request, response: Response) {
    try {
      const user = request.user;
      const [accessToken, refreshToken] = await this.generateTokens(user);
      await this.setTokens(response, { accessToken, refreshToken });
      return user;
    } catch (err) {
      console.log(err);
      throw new HttpException(err.response, err.status);
    }
  }

  public async getUserById(userId: string): Promise<Auth> {
    return this.getUserByField('id', userId);
  }

  public async getUserByEmail(email: string): Promise<Auth> {
    return this.getUserByField('email', email);
  }

  public async getUserByProviderId(providerId: string): Promise<Auth> {
    return this.getUserByField('providerId', providerId);
  }

  private async getAuthenticatedUser(email: string, password: string) {
    try {
      let user = await this.getUserByField('email', email);
      if (!user) {
        user = await this.getUserByField('phone', email);
      }
      if (!user) {
        throw new InvalidCredentials();
      }
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        throw new InvalidCredentials();
      }
      return user;
    } catch (err) {
      throw err;
    }
  }

  private async getUserByField(field: string, value: string | number) {
    const user = await this.authRepository.findOne({
      where: { [field]: value },
    });
    return user;
  }

  private async generateTokens(user) {
    const jwtid = uuidv4();
    const accessToken = await this.jwtService.signAsync(
      {
        id: user.id,
        email: user.email,
      },
      {
        issuer: 'edge',
        secret: this.configService.get('JWT_ACCESS_SECRET_KEY'),
        expiresIn: this.configService.get('JWT_ACCESS_EXPIRATION_TIME'),
      },
    );
    const refreshToken = await this.jwtService.signAsync(
      {
        id: user.id,
        email: user.email,
      },
      {
        jwtid,
        issuer: 'edge',
        secret: this.configService.get('JWT_REFRESH_SECRET_KEY'),
        expiresIn: this.configService.get('JWT_REFRESH_EXPIRATION_TIME'),
      },
    );

    return [accessToken, refreshToken];
  }

  private async setTokens(
    res: Response,
    {
      accessToken,
      refreshToken,
    }: { accessToken: string; refreshToken?: string },
  ) {
    const domain = this.configService.get('DOMAIN');
    res.cookie('access_token', accessToken, {
      maxAge: 1000 * 60 * 60 * 1,
      httpOnly: true,
      sameSite: true,
      domain: domain,
    });

    if (refreshToken) {
      res.cookie('refresh_token', refreshToken, {
        maxAge: 1000 * 60 * 60 * 24 * 30,
        httpOnly: true,
        sameSite: true,
        domain: domain,
      });
    }
  }

  async uploadImage(file) {
    if (!file) {
      throw new NotFoundException('No file uploaded');
    }
    const filePath = `${file.filename}`;
    return {
      message: 'File uploaded successfully',
      url: filePath,
      type: 'image',
    };
  }
}
