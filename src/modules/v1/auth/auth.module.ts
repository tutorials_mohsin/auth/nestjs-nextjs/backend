import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Auth } from './entity/auth.entity';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import {
  FacebookAuthStrategy,
  GithubAuthStrategy,
  GoogleAuthStrategy,
  JwtAuthStrategy,
} from './strategies';
import { MailsenderModule } from '../mailsender/mailsender.module';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    TypeOrmModule.forFeature([Auth]),
    MailsenderModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get('JWT_ACCESS_SECRET_KEY'),
        signOptions: {
          expiresIn: configService.get('JWT_ACCESS_EXPIRATION_TIME'),
        },
      }),
    }),
    MulterModule.register({
      dest: './uploads',
    }),
  ],
  providers: [
    AuthService,
    JwtAuthStrategy,
    GithubAuthStrategy,
    GoogleAuthStrategy,
    FacebookAuthStrategy,
  ],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule { }
