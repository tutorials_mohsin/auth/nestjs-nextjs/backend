import { AbstractEntity } from '../../../../common/entities';

import { Entity, Column, BeforeInsert, BeforeUpdate } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Exclude } from 'class-transformer';
import { Role } from 'src/common/enums';

@Entity('users')
export class Auth extends AbstractEntity<Auth> {
  @Column({
    length: 200,
    name: 'name',
    nullable: false,
  })
  public name: string;

  @Column({
    unique: true,
    length: 200,
    name: 'email',
    nullable: false,
  })
  public email: string;

  @Column({
    length: 200,
    name: 'phone',
    nullable: true,
  })
  public phone: string;

  @Column({
    length: 200,
    name: 'provider',
    nullable: false,
  })
  public provider: string;

  @Column({
    length: 200,
    name: 'provider_id',
    nullable: true,
  })
  public providerId: string;

  @Exclude()
  @Column({
    length: 200,
    name: 'password',
    nullable: false,
  })
  public password: string;

  @Column({
    type: 'enum',
    enum: Role,
    default: Role.USER,
    name: 'role',
  })
  public role: Role;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    const saltRounds = 10;
    this.password = await bcrypt.hash(this.password, saltRounds);
  }
}
