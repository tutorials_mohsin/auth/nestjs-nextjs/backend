import {
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  Post,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { CreateAuthDto, LoginAuthDto } from './dtos';
import { AuthService } from './auth.service';
import {
  AccessTokenGuard,
  FacebookGuard,
  GithubGuard,
  GoogleGuard,
  RoleGuard,
} from './guards';
import { HasRoles } from 'src/common/decorator/roles.decorator';
import { Role } from 'src/common/enums';
import { FileInterceptor } from '@nestjs/platform-express';

@ApiTags('v1/auth')
@Controller({
  path: 'auth',
  version: '1',
})
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @ApiCreatedResponse({
    description: 'Create an account with provided data if correct',
  })
  @Get('local/sendmail')
  async sendMail(
  ) {
    return await this.authService.sendMail();
  }

  @ApiCreatedResponse({
    description: 'Create an account with provided data if correct',
  })
  @Post('local/register')
  async register(
    @Body() createAuthDto: CreateAuthDto,
    @Res() response: Response,
  ) {
    const ret = await this.authService.register(createAuthDto, response);
    return response.send(ret);
  }

  @ApiOkResponse({
    description: 'Logs in user',
  })
  @HttpCode(200)
  @Post('local/login')
  async login(@Body() loginAuthDto: LoginAuthDto, @Res() response: Response) {
    const ret = await this.authService.login(loginAuthDto, response);
    return response.send(ret);
  }

  @ApiOkResponse({
    description: 'Logs in user',
  })
  @ApiBearerAuth()
  @HttpCode(200)
  @UseGuards(AccessTokenGuard)
  @Get('local/isloggedin')
  async isloggedin() {
    return { status: 'success' };
  }

  @ApiOkResponse({
    description: 'Logs in user',
  })
  @ApiBearerAuth()
  @HttpCode(200)
  @HasRoles(Role.MODERATOR, Role.ADMIN)
  @UseGuards(AccessTokenGuard, RoleGuard)
  @Get('local/isadmin')
  async isAdmin() {
    return { status: 'success' };
  }

  @ApiOkResponse({
    description: 'Logs in user google one tap',
  })
  @HttpCode(200)
  @Post('local/googleonetap')
  async googleOneTap(@Body('token') token: string, @Res() response: Response) {
    const ret = await this.authService.googleOneTap(token, response);
    return response.send(ret);
  }

  @ApiOkResponse({
    description: 'Logs in user google one tap',
  })
  @HttpCode(200)
  @Post('local/forgetpassword')
  async forgetPassword(@Body('email') email: string) {
    const ret = await this.authService.forgetPassword(email);
    return { email, status: 'success', message: `An email is sent at ${ret}` };
  }

  @ApiOkResponse({
    description: 'Logs out user',
  })
  @HttpCode(200)
  @UseGuards(AccessTokenGuard)
  @Get('local/logout')
  async logout(@Res() response: Response) {
    response.clearCookie('access_token');
    response.clearCookie('refresh_token');
    return response.send('Cookies cleared');
  }

  @ApiOkResponse({
    description: 'Github login url',
  })
  @HttpCode(200)
  @Get('github/login')
  @UseGuards(GithubGuard)
  async githubLogin() { }

  @ApiOkResponse({
    description: 'Github callback url',
  })
  @HttpCode(200)
  @Get('github/callback')
  @UseGuards(GithubGuard)
  async githubLoginCallback(
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const ret = await this.authService.setLoginCookie(request, response);
    return response.send(ret);
  }

  @ApiOkResponse({
    description: 'Google login url',
  })
  @HttpCode(200)
  @Get('google/login')
  @UseGuards(GoogleGuard)
  async googleLogin() { }

  @ApiOkResponse({
    description: 'Google callback url',
  })
  @HttpCode(200)
  @Get('google/callback')
  @UseGuards(GoogleGuard)
  async googleLoginCallback(
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const ret = await this.authService.setLoginCookie(request, response);
    return response.send(ret);
  }

  @ApiOkResponse({
    description: 'Facebook login url',
  })
  @HttpCode(200)
  @Get('facebook/login')
  @UseGuards(FacebookGuard)
  async facebookLogin() {
    console.log('Facebook login url');
  }

  @ApiOkResponse({
    description: 'Facebook callback url',
  })
  @HttpCode(200)
  @Get('facebook/callback')
  @UseGuards(FacebookGuard)
  async facebookLoginCallback(
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const ret = await this.authService.setLoginCookie(request, response);
    return response.send(ret);
  }

  @Post('/image')
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ summary: 'Upload a single file' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiResponse({
    status: 201,
    description: 'Single File uploaded successfully',
    schema: {
      type: 'object',
      properties: {
        statusCode: { type: 'string', example: 'success' },
      },
    },
  })
  async uploadImageFile(@UploadedFile() file) {
    return this.authService.uploadImage(file);
  }

  @Get('/download/:imageurl')
  async downloadSchedule(
    @Res() response: Response,
    @Param('imageurl') imageurl: string,
  ) {
    response.sendFile(imageurl, { root: 'uploads' });
  }
}
